import PropTypes from "prop-types";
import Button from "../../components/Button/Button";
import './Favorites.scss'

export  function Favorites(props) {
    const { favoriteItems, handleItemFavoriteClose } = props;

    const productsInFavorites = favoriteItems.map((item) => {
        return (
            <div key={item.id} className="card_in-favorites">
                <div className="in-favorites-wrapper"> 
                <h3 className="product-name">{item.name}</h3>
                    <img src={item.image} alt={item.name} />
                    <p className="product-price">{item.price} грн</p>
                </div>
               

                <div className="in-favorites-button">
                    <Button
                        className="btn-favorite"
                        onClick={() => handleItemFavoriteClose(item)}
                    > Cancel
                    </Button>
                </div>
            </div>
        );
    });

    return (
        <div className="favorites-page">
            <h2 className="favorites-page-title">Favorites</h2>
            {productsInFavorites.length === 0 ? (
                <div className="favorites-page-none">
                    No product has been selected.
                </div>
            ) : (
                <div className="favorites-items">{productsInFavorites}</div>
            )}
        </div>
    );
}
Favorites.propTypes = {
    favoriteItems: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            image: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
        })
    ).isRequired,
    handleItemFavoriteClose: PropTypes.func.isRequired,
};