import {Link} from 'react-router-dom'

export function NotFound(){
    return(
        <>
            <h2>not found</h2>
            <Link to="/">RETURN TO HOME PAGE</Link>
        </>
    )
}