import PropTypes from 'prop-types';
import "./Button.scss";

export default function Button({type, classNames, onClick, children}){

    return(
        <button className={classNames} type={type} onClick={onClick} >
            {children}
        </button>
    )
}

Button.propTypes = {
    type: PropTypes.oneOf(['button', 'submit', 'reset']),
    classNames: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node.isRequired
};