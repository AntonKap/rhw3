import PropTypes from 'prop-types';
import './Modal.scss'

export default function ModalWrapper({ children,onConfirm }) {

    function clickFromOutside(ev){
        if (ev.target === ev.currentTarget) {
            onConfirm()
        }
    }
    
    return(
        <div className="modal-wrapper" onClick={clickFromOutside}>
            {children}
        </div>
        )
}
ModalWrapper.propTypes = {
    children: PropTypes.node.isRequired,
    onConfirm: PropTypes.func.isRequired
};
