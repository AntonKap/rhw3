import PropTypes from 'prop-types';
import './Modal.scss'

export default function ModalHeader({children}) {
    
    return(
        <div className="modal-header">
            {children}
        </div>
        )
}
ModalHeader.propTypes = {
    children: PropTypes.node.isRequired
};
