import PropTypes from 'prop-types';
import './Modal.scss'


export default function ModalBody({children}) {
    
    return(
        <div className="modal-body">
            {children}
        </div>
        )

}
ModalBody.propTypes = {
    children: PropTypes.node.isRequired
};