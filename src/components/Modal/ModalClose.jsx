import PropTypes from 'prop-types';
import './Modal.scss'

export default function ModalClose({onClick}) {
    
    return(
        <div className="modal-close" onClick={onClick}>
            &times;
        </div>
        )
}
ModalClose.propTypes = {
    onClick: PropTypes.func.isRequired
};
