import PropTypes from 'prop-types';
import './Modal.scss'

export default function Modal({children}) {
    
    return(
    <div className="modal">
        {children}
    </div>
    )
}
Modal.propTypes = {
    children: PropTypes.node.isRequired
};
