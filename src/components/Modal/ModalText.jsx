import PropTypes from 'prop-types';
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalHeader from "./ModalHeader"
import ModalWrapper from './ModalWrapper'
import ModalClose from './ModalClose'
import Modal from "./Modal"




export default  function ModalText({title, text, firstText, onClose}){

    return(
    <>
        <ModalWrapper onClose={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                    <h2> {title}</h2>
                    <p>Color: {text}</p>

                </ModalBody>
                <ModalFooter
                    firstText = {firstText}
                    firstClick ={onClose}
                />
            </Modal>
        </ModalWrapper>
    </>
    )
}
ModalText.propTypes = {
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    firstText: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired
};
